export const PT_LANG = {
     FAVORITES: 'Favoritos',
     LOADING_NEW_ITEMS: 'Carregando novos itens',
     PORTUGUESE: 'Português',
     ENGLISH: 'Inglês',
     ADD_TO_FAVORITES: 'Adicionar aos Favoritos'
}