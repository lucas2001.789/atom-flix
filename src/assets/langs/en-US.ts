import { PT_LANG } from './pt-BR';

export const EN_LANGUAGE: typeof PT_LANG = {
     FAVORITES: 'Favorites',
     LOADING_NEW_ITEMS: 'Loading new items',
     PORTUGUESE: 'Portuguese',
     ENGLISH: 'English',
     ADD_TO_FAVORITES: 'Add to Favorites'
}