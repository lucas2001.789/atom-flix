import bookmarkFilled from './bookmark-filled.svg';
import bookmarkOutline from './bookmark-outline.svg';
import dotIcon from './dot-icon.svg';
import favorite from './favorite.svg';
import favoriteFilled from './favorite_filled.svg';
import favoriteOutline from './favorite_outline.svg';

export const icons = {
     favorite,
     bookmarkFilled,
     bookmarkOutline,
     favoriteFilled,
     favoriteOutline,
     dotIcon
}