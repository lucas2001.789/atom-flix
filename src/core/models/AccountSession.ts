export interface AccountSession {
     success: boolean,
     session_id: string;
}