export interface IAccountListCheck {
     favourite_ids: string[],
     rated_ids: string[],
     ratings: unknown[],
     watchlist_ids: string[]
}